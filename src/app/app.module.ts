import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { AuthModule } from './auth/auth.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UsuarioComponent } from './usuario/usuario.component';
import { GastoComponent } from './gasto/gasto.component';
import { CompraComponent } from './compra/compra.component';
import { TareaComponent } from './tarea/tarea.component';
import { CalendarioComponent } from './calendario/calendario.component';
import { PageNotFoundComponent } from './not-found.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    UsuarioComponent,
    GastoComponent,
    CompraComponent,
    TareaComponent,
    CalendarioComponent,
    PageNotFoundComponent,
    NavbarComponent,
    HomeComponent
  ],
  imports: [
    AuthModule,
    AppRoutingModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
