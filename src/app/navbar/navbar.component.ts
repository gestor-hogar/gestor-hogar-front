import { Component, OnInit } from '@angular/core';
import { Usuario } from '../auth/usuario';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  usuario = this.authService.getUsuario();

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  logout() {
    // Cerrar Sesión
    this.authService.logout();
  }

}
