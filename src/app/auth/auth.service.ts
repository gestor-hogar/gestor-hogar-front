import { Injectable } from '@angular/core';
import { Usuario } from "./usuario";
import { Http, Response, URLSearchParams, RequestOptions, Headers } from "@angular/http";
import { Router } from '@angular/router';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {

  private apiUrl = 'http://localhost:8080/api/login';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http, private router: Router) {
  }

    login(email: string, password: string) {
        let params = new URLSearchParams();
        params.append('email', email);
        params.append('password', password);	
        let options = new RequestOptions({ headers: this.headers, params: params });
        
        return this.http.get(this.apiUrl, options)
            .map((response:Response) => {
                // login successful if there's a jwt token in the response
                //let token = response && response.json() && response.json().token;
                let usuario = response && response.json();
                if (usuario.email) {
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ usuario: usuario}));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            }).catch((error:any) => Observable.throw(error.json().message || 'Server error'))
    }

    getToken(): String {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser && currentUser.token;
        return token ? token : "";
    }

    getUsuario(): Usuario {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var usuario = currentUser && currentUser.usuario;
        return usuario ? usuario : {};
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        // Ir a la ventana de login
        this.router.navigate(['/login']);
    }

    remember(email: string) {
        let options = new RequestOptions({ headers: this.headers});
        return this.http.get(this.apiUrl + "/" + email + "/remember", options)
            .map((response:Response) => {
                return;
            }).catch((error:any) => Observable.throw(error.json().message || 'Server error'))
    }

    register(usuario: object) {
        let options = new RequestOptions({ headers: this.headers});
        return this.http.post(this.apiUrl, usuario, options)
            .map((response:Response) => {
                return;
            }).catch((error:any) => Observable.throw(error.json().message || 'Server error'))
    }
}