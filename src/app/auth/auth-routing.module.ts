import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthComponent }    from './auth.component';
import { LoginComponent }    from './login/login.component';
import { RememberComponent }  from './remember/remember.component';
import { RegisterComponent }  from './register/register.component';

const authRoutes: Routes = [
  { path: 'login',  
    component: AuthComponent,
    children: [
      { path: '', component: LoginComponent },
      { path: 'remember', component: RememberComponent },
      { path: 'register', component: RegisterComponent }
    ]
  }/*,
  { path: '',   redirectTo: '/login', pathMatch: 'full' },*/
];

@NgModule({
  imports: [
    RouterModule.forChild(authRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRoutingModule { }
