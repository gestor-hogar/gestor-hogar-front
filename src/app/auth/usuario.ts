export class Usuario {

  email: string;
  password: string;
  password2: string;
  nombre: string;
  telefono: string;
  idFamilia: string;
  nombreFamilia: string;

  constructor(email: string, password: string, password2: string, nombre: string, telefono: string,
    idFamilia: string, nombreFamilia: string){
    this.email = email;
    this.password = password;
    this.password2 = password2;
    this.nombre = nombre;
    this.telefono = telefono;
    this.idFamilia = idFamilia;
    this.nombreFamilia = nombreFamilia;
  }

}