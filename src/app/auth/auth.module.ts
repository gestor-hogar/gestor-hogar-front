import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, FormBuilder } from '@angular/forms';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RememberComponent } from './remember/remember.component';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthService } from './auth.service';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    HttpModule,
    FormsModule
  ],
  declarations: [
    AuthComponent,
    LoginComponent, 
    RegisterComponent, 
    RememberComponent],
  providers: [ AuthService, FormBuilder ]
})
export class AuthModule { }
