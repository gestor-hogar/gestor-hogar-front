import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    if (this.model.password !== this.model.password2)
      return '';
    this.loading = true;
    this.authService.register(this.model)
        .subscribe(result => {
            this.router.navigate(['/login']);
        }, error => {
          this.loading = false;
          this.error = error;
        });
  }

}
