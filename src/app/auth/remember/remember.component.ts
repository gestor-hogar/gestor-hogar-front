import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-remember',
  templateUrl: './remember.component.html',
  styleUrls: ['./remember.component.css']
})
export class RememberComponent implements OnInit {

  model: any = {};
  loading = false;
  error = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {  }

  remember() {
    this.loading = true;
    this.authService.remember(this.model.email)
        .subscribe(result => {
            this.router.navigate(['login']);
        }, error => {
          this.loading = false;
          this.error = error;
        });
  }

}
