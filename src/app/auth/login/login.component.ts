import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() { }

  login() {
    this.loading = true;
    this.authService.login(this.model.email, this.model.password)
        .subscribe(result => {
            if (result === true) {
                // login successful
                this.router.navigate(['home']);
            } else {
                // login failed
                this.error = 'Email o contraseña son incorrectos.';
                this.loading = false;
            }
        }, error => {
          this.loading = false;
          this.error = error;
        });
  }

}
